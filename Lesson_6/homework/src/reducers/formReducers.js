import { SET_FROM } from "../constants";

initialState = {
  firstName: "",
  lastName: "",
  education: {
    university: "",
    city: "",
    country: ""
  },
  address: {
    country: "",
    city: "",
    street: ""
  }
};

function formReducers(state = initialState, action) {
  switch (action.state) {
    case SET_FORM:
      let { firstName, lastName, education, address } = action.data;
      return {
        ...state,
        firstName,
        lastName,
        education,
        address
      };

    default:
      return state;
  }
}

export default formReducers;
