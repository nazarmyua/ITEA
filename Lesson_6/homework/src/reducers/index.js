import {combineReducers} from "redux"
import artistReducers from "./artistReducers"
import formReducers from "./formReducers"

let reducers = combineReducers({
    artistReducers,
    formReducers
})
export default reducers