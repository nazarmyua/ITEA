import { REQ_ARTIST, RES_ARTIST, ERROR_ARTIST, SET_STARS } from "../constants";

const initialState = {
  loading: false,
  loaded: false,
  data: [],
  error: ""
};

function artistResucers(state = initialState, action) {
  switch (action.type) {
    case REQ_ARTIST:
      return {
        ...state,
        loading: true
      };
    case RES_ARTIST:
      return {
        ...state,
        data: action.data,
        loading: false,
        loaded: true
      };
    case ERROR_ARTIST:
      return {
        ...state,
        errors: action.error
      };
    case SET_STARS:
      return {
        ...state,
        data: state.data.map(artist => {
          if (artist.name.toLowerCase() != action.name.toLowerCase()) {
            return artist;
          } else {
            artist.album = artist.album.map(alb => {
              if (alb.name.toLowerCase() != action.album.toLowerCase()) {
                return alb;
              } else {
                alb.compositions = alb.compositions.map(composition => {
                  if (
                    composition.name.toLowerCase() !=
                    action.compositionName.toLowerCase()
                  ) {
                    return composition;
                  } else {
                    composition["stars"] = action.stars;
                    return composition;
                  }
                });
                return alb;
              }
            });
            return artist;
          }
        })
      };

    default:
      return state;
  }
}

export default artistResucers;
