import { REQ_ARTIST, RES_ARTIST, ERROR_ARTIST,SET_STARS } from "../constants";
import axios from "axios"

const artistActionCreator = {
  getAtrist() {
    return function(dispatch, getState) {
      const url =
        " http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2";
      dispatch({
        type: "PROMISE",
        actions: [REQ_ARTIST, RES_ARTIST, ERROR_ARTIST],
        promise: axios.get(url)
      });
    };
  },
  setStars(name, album, compositionName) {
    return function(dispatch, getState) {
      dispatch({
        type: SET_STARS,
        name,
        album,
        compositionName,
        stars: 3
      })
    };
  }
};

export default artistActionCreator;
