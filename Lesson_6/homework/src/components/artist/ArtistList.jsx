import { connect } from "react-redux";
import React, { Component } from "react";
import  artistActionCreator from "../../actions/artistActionCreator";
import { Link } from "react-router-dom";
import {Helmet} from "react-helmet"

class ArtistList extends Component {
  componentDidMount = () => {
    let { getArtist, data, loaded } = this.props;
    if (!data || !loaded) {
      console.log("test");
      getArtist();
    }
  };

  render() {
    let { data, loading, loaded } = this.props;
    if (loading) return (<div>Loading</div>);
    if (loaded)
      return (
        <div>
        <Helmet>
          <title>MyMusicBox</title>
        </Helmet>
          <ul>
            {data.map((artist, key) => {
              return (
                <li key={key}>
                  <Link to={`/artist/${artist.name}`}>{artist.name}</Link>
                </li>
              );
            })}
          </ul>
        </div>
      );
    if (!loading && !loaded) return (<div>placeholder</div>);
  }
}

const mapStateToProps = (state, ownProps) => {
  let { data, loading, loaded, error } = state.artistReducers;
  return {
    data,
    loading,
    loaded,
    error
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getArtist: () => {
        artistActionCreator.getAtrist()(dispatch, ownProps);
    },
  };
};

const ArtistListConected = connect(mapStateToProps, mapDispatchToProps)(
  ArtistList
);

export default ArtistListConected;
