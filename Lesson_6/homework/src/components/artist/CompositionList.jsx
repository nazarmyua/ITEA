import { connect } from "react-redux";
import React, { Component } from "react";
import artistActionCreator from "../../actions/artistActionCreator";
import { Helmet } from "react-helmet";

class CompositionList extends Component {
  constructor() {
    super();
    this.setStar = this.setStar.bind(this);
  }
  componentDidMount() {
    let { getArtist, data, loaded } = this.props;
    if (!data || !loaded) {
      getArtist();
    }
  }

  setStar(e) {
    let { setStars } = this.props;
    let { name, album } = this.props.match.params;
    let compositionName = e.target.dataset.compositionname;
    setStars(name, album, compositionName);
  }

  render() {
    let { data, loading, loaded } = this.props;
    let { name, album } = this.props.match.params;
    if (loading) {
      return <div>Loading</div>;
    }
    if (!loaded) {
      return <div>Loaded</div>;
    }
    if (loaded && data) {
      let curentArtist = data.filter(item => {
        return item.name.toLowerCase() == name.toLowerCase();
      })[0];
      let curentAlbum = curentArtist.album.filter(item => {
        return item.name.toLowerCase() == album.toLowerCase();
      })[0];
      return (
        <div>
          <Helmet>
            <title>MyMusicBox:{name}-{album}</title>
          </Helmet>
          <ul>
            {curentAlbum.compositions.map((item, index) => {
              return (
                <li key={index}>
                  {item.name} {item.duration}
                  <button
                    data-compositionname={item.name}
                    onClick={this.setStar}
                  >
                    Set star
                  </button>
                  <b>{item.stars}</b>
                </li>
              );
            })}
          </ul>
        </div>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  let { data, loading, loaded, error } = state.artistReducers;
  return {
    data,
    loading,
    loaded,
    error
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getArtist: () => {
      artistActionCreator.getAtrist()(dispatch, ownProps);
    },
    setStars: (name, album, compositionName) => {
      artistActionCreator.setStars(name, album, compositionName)(
        dispatch,
        ownProps
      );
    }
  };
};

const CompositionListConnected = connect(mapStateToProps, mapDispatchToProps)(
  CompositionList
);

export default CompositionListConnected;
