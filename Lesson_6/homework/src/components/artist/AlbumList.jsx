import { connect } from "react-redux";
import React, { Component } from "react";
import artistActionCreator from "../../actions/artistActionCreator";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

class AlbumList extends Component {
  componentDidMount = () => {
    let { getArtist, data, loaded } = this.props;
    if (!data || !loaded) {
      console.log("test");
      getArtist();
    }
  };

  render() {
    let { data, loading, loaded } = this.props;
    let artistName = this.props.match.params.name;
    if (loading) {
      return <div>Loading</div>;
    }
    if (!loaded) {
      return <div>Loaded</div>;
    }
    if (loaded && data) {
      let artist = data.filter(item => {
        return item.name == artistName;
      });
      return (
        <div>
          <Helmet>
            <title>MyMusicBox:{artistName}</title>
          </Helmet>
          <ul>
            {artist[0].album.map((item, index) => {
              let sumDuration = item.compositions.reduce(
                (previusValue, curentItem) => {
                  return (
                    previusValue +
                    curentItem.duration.split(":")[0] * 60 +
                    --curentItem.duration.split(":")[1]
                  );
                },
                0
              );
              return (
                <li key={index}>
                  <Link to={`/artist/${artistName}/${item.name}`}>
                    {item.name} ({parseInt(sumDuration / 60)}:{sumDuration % 60})
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  let { data, loading, loaded, error } = state.artistReducers;
  return {
    data,
    loading,
    loaded,
    error
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getArtist: () => {
      artistActionCreator.getAtrist()(dispatch, ownProps);
    }
  };
};

const AlbumListConected = connect(mapStateToProps, mapDispatchToProps)(
  AlbumList
);

export default AlbumListConected;
