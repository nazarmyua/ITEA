import { createStore, applyMiddleware, compose } from "redux";
import { RES_ARTIST } from "./constants";
import reducer from "./reducers";

import thunk from "redux-thunk";

import promise from "./middleware/promise";

const changeHandler = () => {
  let state = store.getState();
  localStorage.setItem("data", JSON.stringify(state.artistReducers.data));
};
const composeEnhancers =
  process.env.NODE_ENV !== "production" &&
  typeof window !== "undefined" &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose;

const middleware = applyMiddleware(thunk, promise);

let store;
if (localStorage.getItem("data")!==null) {
  let tempData ={artistReducers: { data: JSON.parse(localStorage.getItem("data")), loaded: true }};
  console.log(tempData)
  store = createStore(reducer, tempData, composeEnhancers(middleware));
} else {
  store = createStore(reducer, composeEnhancers(middleware));
}
store.subscribe(changeHandler);

export default store;
