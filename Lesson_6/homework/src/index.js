import React from "react";
import ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
import Routes from "./routes";

import LayoutRoute from "./components/Layout";
import SimpleLayoutRoute from "./components/SimpleLayout";
import NotFound from "./components/NotFound";

import { Provider } from "react-redux";
import store from "./store";

import { BrowserRouter, Switch } from "react-router-dom";
const supportsHistory = "pushState" in window.history;

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter forceRefresh={!supportsHistory}>
      <div>
        <Switch>
          {Routes.map((route, index) => {
            return <LayoutRoute key="index" {...route} />;
          })}
          <SimpleLayoutRoute path="*" exact component={NotFound}/>
        </Switch>       
      </div>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
