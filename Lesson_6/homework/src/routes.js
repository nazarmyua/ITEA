import Home from "./components/Home";
import About from "./components/About";

import ArtistList from "./components/artist/ArtistList";
import AlbumList from "./components/artist/AlbumList";
import CompositionList from "./components/artist/CompositionList";

const Routes = [
  {
    path: "/",
    component: Home,
    exact: true
  },
  {
    path: "/about",
    component: About
  },
  {
    path: "/artist/:name/:album",
    component: CompositionList,
    exact: true
  },
  {
    path: "/artist/:name",
    component: AlbumList
  },
  {
    path: "/artist",
    component: ArtistList
  }
];

export default Routes;
