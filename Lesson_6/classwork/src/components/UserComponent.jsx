import { connect } from 'react-redux';
import React, { Component } from 'react';
import { GET_USER } from "../constants"
import { getUserFrom } from "../actions/userActionCreator.js"

class UserList extends Component {

    componentDidMount = () => {
        let {getUser} = this.props
        getUser();
    }

    render() {
        let {data} = this.props
        return (
            <div>
            {
                data.map((item,key)=>(<div key={key}>{item.name}</div>))
            }
            </div>
        );
    }
}

const mapStateToProps = (state,ownProps) => {
    let { data, loading, loaded, error} = state.userReduser
    return {
        data,
        loading,
        loaded,
        error
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getUser : () => {
            console.log("state 1")
            getUserFrom()(dispatch,ownProps)
        }
    }
}

const UserListConected = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserList)

export default UserListConected;