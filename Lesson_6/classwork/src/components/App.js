import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Switch, Route, Link, NavLink } from 'react-router-dom';

class App extends Component {
  render() {
    return(
      <div className="start">
        <img className="App-logo" width="100" src={logo}/>
        <h1>Clean React-Redux Project with React Router 4</h1>
      </div>
    )
  }
}

export default App;
