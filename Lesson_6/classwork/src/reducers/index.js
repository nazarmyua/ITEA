import { combineReducers } from 'redux';
import userReduser from "./UserReduser"

const reducer = combineReducers({
  userReduser
});

export default reducer;
