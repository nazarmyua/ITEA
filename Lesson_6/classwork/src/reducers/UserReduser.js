import { REQUEST, RESPONSE, ERROR } from "../constants";

const initialState = {
  loading: false,
  loaded: false,
  data: [],
  error: ""
};

function userReduser(state = initialState, action) {
  switch (action.type) {
    case REQUEST:
      return {
          ...state,
          loading: true
      };
    case RESPONSE:
    return {
        ...state,
        data: action.data,
        loading: false,
        loaded: true
      };
    case ERROR:
    return {
        ...state,
        errors: action.error
       };


    default:
      return state;
  }
}

export default userReduser;
