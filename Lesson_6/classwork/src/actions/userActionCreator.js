import { REQUEST,RESPONSE,ERROR } from "../constants"

export const getUserFrom = () => {

    const url = "http://www.json-generator.com/api/json/get/bTCOusPgKq?indent=2"  

    return function (dispatch, getState ) {
        console.log(dispatch)
        dispatch({
          type: 'REQUEST',
          data: fetch(url)
            .then(res => res.json() )
            .then(response => {

              dispatch({
                type: 'RESPONSE',
                data: response
              });

            }).catch( err => {
              dispatch({
                type: 'ERROR',
                error: err
              });
            })
        });
    }
} 