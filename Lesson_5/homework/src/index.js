import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import {Provider} from "react-redux"
import store from "./store"

import { BrowserRouter , Switch, Route, Link } from 'react-router-dom';

import Home from "./components/Home"
import About from "./components/About"
import ToDoList from "./components/ToDoList"

const supportsHistory = 'pushState' in window.history;

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter  basepath="/" forceRefrech={!supportsHistory}>
            <Switch>
                <Route path="/" component={ToDoList}/>
            </Switch>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
