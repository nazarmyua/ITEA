import { connect } from 'react-redux';
import React, { Component } from 'react';
import { TODOLIST_ADD } from "../constants"

class toDoList extends Component {
    constructor (){
        super();
        this.state={
            input:"",
        }
        this.onclickhandler = this.onclickhandler.bind(this)
        this.onValueChange = this.onValueChange.bind(this)
    }
    onclickhandler (){
        let {toDoListAdd} = this.props
        toDoListAdd(this.state.input);
    }

    onValueChange (e) {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value 
        })
    }

    render() {
        let {toDoList} = this.props;
        let {input} = this.state

        return (
            <div>
            {
                toDoList.map(( item,key )=>{
                    return (<div>{item}</div>)
                })
            }
            <input type="text" value={input} name={"input"} onChange={this.onValueChange}/>
            <button onClick={ this.onclickhandler } >ADD</button>
            </div>            
        );
    }
}

const mapStateToProps = (state,ownProps) => {
    return {
        toDoList: state.toDoListReduser.toDoList
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        toDoListAdd : ( newToDo ) => {
            dispatch({
                type: TODOLIST_ADD,
                item: newToDo
            })
        }
    }
}

const ToDoList = connect(
    mapStateToProps,
    mapDispatchToProps
)(toDoList)

export default ToDoList;