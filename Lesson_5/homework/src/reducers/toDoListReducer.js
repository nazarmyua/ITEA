import { TODOLIST_ADD, TODOLIST_REMOVE } from "../constants"

const toDoListInitialState = {
    toDoList: ["test","test","testss"]
}

function toDoListReducer (state = toDoListInitialState, action) {
    switch (action.type) {
        case TODOLIST_ADD:
            return {
                ...state,
                toDoList: [
                    ...state.toDoList,
                    action.item
                ]
            }
        default:
            return state
    }
}

export default toDoListReducer;