import React, { Component } from "react";
import "./App.css";
import Toggle from "./components/togge/Toggle";
import ToggleItem from "./components/togge/ToggleItem";
import Input from "./components/input/Input";
import Button from "./components/button/Button"

let genders = [
  { nameView: "Male", value: "male" },
  { nameView: "Female", value: "female" }
];
let layouts = [
  { nameView: "Left", value: "left" },
  { nameView: "Center", value: "center" },
  { nameView: "Right", value: "right" },
  { nameView: "Baseline", value: "baseline" }
];
class App extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        gender: "male",
        layout: "left"
      }
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onSubmitHendler = this.onSubmitHendler.bind(this);
  }

  onChangeHandler(e) {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
      data: {
        ...this.state.data,
        [name]: value
      }
    });
  }

  onSubmitHendler(e) {
    e.preventDefault()
    console.log(this.state.data)
  }

  render() {
    let { gender, layout } = this.state.data;
    let { onChangeHandler, onSubmitHendler } = this;
    return (
      <div className="App">
      <form onSubmit={onSubmitHendler}>
        <Input
          onChangeHandler={onChangeHandler}
          type="text"
          placeholder="Name"
          name="name"
          nameView="Name"
        />
        <Input
          onChangeHandler={onChangeHandler}
          type="password"
          placeholder="Password"
          name="password"
          nameView="Passwords"
        />
        <Toggle
          nameView="Select gender"
          activeValue={gender}
          action={onChangeHandler}
        >
          {genders.map((item, key) => {
            return (
              <ToggleItem
                key={key}
                nameView={item.nameView}
                value={item.value}
                name={"gender"}
              />
            );
          })}
        </Toggle>
        <Input
          onChangeHandler={onChangeHandler}
          type="number"
          placeholder="Test"
          name="age"
          nameView="Age"
        />
        <Toggle
          nameView="Select layout"
          activeValue={layout}
          action={onChangeHandler}
        >
          {layouts.map((item, key) => {
            return (
              <ToggleItem
                key={key}
                nameView={item.nameView}
                value={item.value}
                name={"layout"}
              />
            );
          })}
        </Toggle>
        <Input
          onChangeHandler={onChangeHandler}
          type="text"
          placeholder="Enater your favorite language"
          name="language"
          nameView="Favorite language"
        />
        <Button type="submit" nameView="Submit"/>
        </form>
      </div>
    );
  }
}

export default App;
