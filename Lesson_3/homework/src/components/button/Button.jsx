import React from 'react';

const Button = ({type,nameView}) => {
    return (
        <label>
            <div></div>
            <button type={type}>{nameView}</button>
        </label>
    );
}

export default Button;