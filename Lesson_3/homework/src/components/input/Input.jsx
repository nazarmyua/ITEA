import React from "react";
import PropTypes from 'prop-types';

const Input = ({nameView, name, type, placeholder, value, onChangeHandler}) => {
  return (
    <label>
      <div>{nameView}</div>
      <input
        name={name}
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChangeHandler}
      />
    </label>
  );
};

Input.propTypes = {
  type: PropTypes.oneOf(["text","password","number"]).isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.any,
  onChangeHandler: PropTypes.func,
}

export default Input;
