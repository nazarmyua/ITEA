import React from 'react';

const Toggle = ({children,activeValue,nameView,action}) => {
    return (
        <label>
            <div>{nameView}</div>
            {
                React.Children.map(children,(item)=>{
                    if (item.props.value === activeValue) {
                        return React.cloneElement(item, {...item.props, active: true})
                    } else {
                        return React.cloneElement(item, {...item.props, action: action})
                    }
                })
            }
        </label>
    )
}

export default Toggle;
