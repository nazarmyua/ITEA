import React from 'react';

const ToggleItem = ({name,nameView,value,action,active}) => {
    return (
        <button type="button" name={name} value={value} onClick={action}>{nameView} {active?"true":"false"}</button>
    );
}

export default ToggleItem;