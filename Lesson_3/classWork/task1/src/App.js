import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './components/Toggle/Toggle'
import './components/Toggle/ToggleItem'
import Toggle from './components/Toggle/Toggle';
import ToggleItem from './components/Toggle/ToggleItem';

let genders = [{key: "male", nameView: "Male"},{key: "famale", nameView: "Famale"}]
let layouts = [
  {key: "baseline", nameView: "baseline"},
  {key: "center", nameView: "center"},
  {key: "left", nameView: "left"},
  {key: "right", nameView: "right"}
]

class App extends Component {
  constructor(){
    super();

    this.state={
      data: {
        gender: "male",
        layout: "left"
      }
    }
    this.onToggleChanhe = this.onToggleChanhe.bind(this)
  }

  onToggleChanhe (e) {
    let name = e.target.dataset.name
    let key = e.target.dataset.key
    this.setState({
      data: {
        ...this.state.data,
        [name]:key
      }
  })
    console.log(e.target.dataset.key)
  }

  render() {
    let {gender,layout} = this.state.data
    return (
      <div className="App">

        <Toggle
          activeToggle={gender}
          name="gender"
          nameView="Select gender"
          toggleChenge={this.onToggleChanhe}
        >
          {genders.map((item,key)=><ToggleItem key={key} nameView={item.nameView} keyItem={item.key}/>)}
        </Toggle>

        <Toggle
          activeToggle={layout}
          name="layout"
          nameView="Select layout"
          toggleChenge={this.onToggleChanhe}
        >
          {layouts.map((item,key)=><ToggleItem key={key} nameView={item.nameView} keyItem={item.key}/>)}
        </Toggle>
      </div>
    );
  }
}

export default App;
