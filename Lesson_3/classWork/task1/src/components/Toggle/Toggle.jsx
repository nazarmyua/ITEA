import React from 'react';
const Toggle = ({children, activeToggle, name, nameView, toggleChenge}) => {
    return (
        <div>
            <header>{nameView}</header>
            <section>
                {React.Children.map(children,(toggleItem)=>{
                    console.log(toggleItem.props.keyItem === activeToggle)
                    if(toggleItem.props.keyItem === activeToggle)
                    {
                        return React.cloneElement(toggleItem,{
                            ...toggleItem.props,
                            active: true,
                        })
                    } else {
                        return React.cloneElement( toggleItem,{
                            ...toggleItem.props,
                            changeActiveToggle: toggleChenge,
                            name: name
                        })
                    }
                })}
            </section>
        </div>        
    );
}

export default Toggle;