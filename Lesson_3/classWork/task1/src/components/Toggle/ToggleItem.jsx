import React from 'react';

const ToggleItem = ({nameView, active, name, keyItem, changeActiveToggle}) => {
    return (
        <button data-key={keyItem} data-name={name} onClick={changeActiveToggle}>{nameView} + {active?"true":"false"}</button>
    );
}

export default ToggleItem;