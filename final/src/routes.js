import FilmList from "./components/filmList/FilmList"
import Order from "./components/order/Order"

const Routes = [
  {
    path: "/",
    component: FilmList,
    exact: true
  },
  {
    path: "/order/:guid/:hall",
    component: Order,
    exact: true
  }
];

export default Routes;
