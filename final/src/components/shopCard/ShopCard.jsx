import { connect } from "react-redux";
import React, { Component } from "react";
import "./shopCard.css";
import shopCardActionCreator from "../../actions/shopCardActionCreator";

class ShopCard extends Component {
  constructor() {
    super();
    this.deleteOrder = this.deleteOrder.bind(this);
    this.submit = this.submit.bind(this);
  }
  deleteOrder(e) {
    let { removeOrder } = this.props;
    let { row, place, guid } = e.target.dataset;
    removeOrder(row, place, guid);
  }
  submit() {
    let { shopCard, removeOrder, guid } = this.props;
    let currentOrder = shopCard.filter(item => {
      return item.guid == guid;
    });

    currentOrder.forEach(element => {
      removeOrder(element.row, element.place, element.guid);
    });

    let ordered = localStorage.getItem("ordered");

    if (ordered != null && ordered.length > 0)
      currentOrder = [...JSON.parse(ordered), ...currentOrder];

    localStorage.setItem("ordered", JSON.stringify(currentOrder));
  }
  render() {
    let sum = 0;
    let { shopCard, guid, halls, hallId } = this.props;
    let { deleteOrder, submit } = this;

    if (shopCard) {
      return (
        <div>
          <table>
            <thead>
              <tr>
                <th />
                <th>Row</th>
                <th>Place</th>
                <th>Price</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {shopCard
                .filter(item => {
                  return item.guid == guid;
                })
                .map((item, index) => {
                  let curentPrice = halls
                            .filter(hall => {
                              return hall.index == hallId;
                            })[0]
                            .prices.filter(price => {
                              return price.rows.indexOf(+item.row) >= 0;
                            })[0].price
                            sum+=curentPrice;
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{item.row}</td>
                      <td>{item.place}</td>
                      <td>
                        {
                          curentPrice
                        }
                      </td>
                      <td>
                        <button
                          data-row={item.row}
                          data-place={item.place}
                          data-guid={guid}
                          onClick={deleteOrder}
                        >
                          X
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3" />
                <td>{sum}</td>
                <td />
              </tr>
            </tfoot>
          </table>
          <button onClick={submit}>Submit</button>
        </div>
      );
    }
    return <div>Shop Card</div>;
  }
}

const mapStateToProps = (state, ownProps) => {
  let { shopCard } = state.shopCardReducers;
  let halls = state.hallReducers.data;
  return {
    shopCard,
    halls
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    removeOrder: (row, place, guid) => {
      shopCardActionCreator.removeOrder(row, place, guid)(dispatch, ownProps);
    }
  };
};

const ShopCardConected = connect(mapStateToProps, mapDispatchToProps)(ShopCard);

export default ShopCardConected;
