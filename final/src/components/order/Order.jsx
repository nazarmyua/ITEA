import React from "react";
import Hall from "../hall/Hall";
import ShopCard from "../shopCard/ShopCard";
import "./order.css";

const Order = ({ match }) => {
  return (
    <div className="orderWrapper">
      <ShopCard hallId={match.params.hall} guid={match.params.guid} />
      <Hall hallId={match.params.hall} guid={match.params.guid} />
    </div>
  );
};

export default Order;
