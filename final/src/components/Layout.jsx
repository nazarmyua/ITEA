import React from "react";
import { NavLink, Route } from "react-router-dom";
import {Helmet} from "react-helmet"

const Layout = ({ children }) => {
  return (
    <div>
      <div>Main layout</div>
      <div>
        <p>Navigation</p>
        <ul>
          <li><NavLink to="/">Home</NavLink></li>
        </ul>
      </div>
      <div>{children}</div>
    </div>
  );
};

const LayoutRoute = ({ component: Component, ...routeParams }) => {
  return (
    <Route
      {...routeParams}
      render={match => (
        <Layout>
          <Component {...match} />
        </Layout>
      )}
    />
  );
};

export default LayoutRoute;
