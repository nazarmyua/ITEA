import { connect } from "react-redux";
import React, { Component } from "react";
import filmListActionCreator from "../../actions/filmListActionCreator";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

class FilmList extends Component {
  componentDidMount = () => {
    let { getFilmList, data, loaded } = this.props;
    if (!data || !loaded) {
      getFilmList();
    }
  };

  render() {
    let { data, loading, loaded } = this.props;
    if (loading) {
      return <div>Loading</div>;
    }
    if (!loaded) {
      return <div>Loaded</div>;
    }
    if (loaded && data) {
      return (
        <div>

          <ul>
            {data.map((item, index) => {
              return (
                <li key={index}>
                  <div>{item.name}</div>
                  <div>
                    {
                      item.times.sort((p, n) => p.time > n.time).map((time, key) => {
                      return <div key={key}><Link to={`/order/${time.guid}/${time.hall}`}>{time.time}</Link></div>;
                      })
                    }
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  let { data, loading, loaded, error } = state.filmListReducers;
  return {
    data,
    loading,
    loaded,
    error
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getFilmList: () => {
      filmListActionCreator.getFilmList()(dispatch, ownProps);
    }
  };
};

const FilmListConected = connect(mapStateToProps, mapDispatchToProps)(FilmList);

export default FilmListConected;
