import { connect } from "react-redux";
import React, { Component } from "react";
import hallActionCreator from "../../actions/hallActionCreator";
import shopCardActionCreator from "../../actions/shopCardActionCreator";
import { Helmet } from "react-helmet";
import Place from "./Place";
import "./hall.css";

class Halls extends Component {
  constructor() {
    super();
    this.reservePlace = this.reservePlace.bind(this);
    this.isOrdered = this.isOrdered.bind(this);
  }
  componentDidMount = () => {
    let { getHalls, data, loaded } = this.props;
    if (!data || !loaded) {
      getHalls();
    }
  };
  reservePlace(e) {
    let { addOrder } = this.props;
    let { place, guid, row } = e.target.dataset;
    addOrder(row, place, guid);
  }
  isDisablePlace(row, place, guid) {
    let ordered = localStorage.getItem("ordered")
    if (ordered == null || ordered.length==0) return false;
    ordered = JSON.parse(ordered);
    return (
      ordered.filter(item => {
        return item.row == row && item.place == place && item.guid == guid;
      }).length > 0
    );
  }
  isOrdered(row, place, guid) {
    let { shopCard } = this.props;
    return (
      shopCard.filter(item => {
        return item.row == row && item.place == place && item.guid == guid;
      }).length > 0
    );
  }
  render() {
    let { data, loading, loaded, hallId, guid } = this.props;
    let { reservePlace, isDisablePlace, isOrdered } = this;
    if (loading) {
      return <div>Loading</div>;
    }
    if (!loaded) {
      return <div>Loaded</div>;
    }
    if (loaded && data) {
      return (
        <div>
          <div className="leftPart">
            {data
              .filter(hall => hall.index == hallId)[0]
              .hallParams.map((item, key) => {
                return (
                  <div key={key}>
                    <span>{key + 1}</span>
                  </div>
                );
              })}
          </div>
          <div className="hall">
            {data
              .filter(hall => hall.index == hallId)[0]
              .hallParams.map((item, key) => {
                return (
                  <div key={key}>
                    {new Array(item.places)
                      .fill(0)
                      .map((item, index) => (
                        <Place
                          isOrdered={isOrdered(key + 1, index + 1, guid)}
                          disable={isDisablePlace(key + 1, index + 1, guid)}
                          key={index}
                          guid={guid}
                          row={key + 1}
                          place={index + 1}
                          action={reservePlace}
                        />
                      ))}
                  </div>
                );
              })}
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  let { data, loading, loaded, error } = state.hallReducers;
  let { shopCard } = state.shopCardReducers;
  return {
    data,
    loading,
    loaded,
    error,
    shopCard
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getHalls: () => {
      hallActionCreator.getHalls()(dispatch, ownProps);
    },
    addOrder: (row, place, guid) => {
      shopCardActionCreator.addOrder(row, place, guid)(dispatch, ownProps);
    }
  };
};

const HallsConected = connect(mapStateToProps, mapDispatchToProps)(Halls);

export default HallsConected;
