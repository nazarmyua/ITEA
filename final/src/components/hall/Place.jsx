import React from "react";
import "./place.css";

const Place = ({ row, place, guid, action, disable, isOrdered }) => {
  return (
    <span
      data-row={row}
      data-place={place}
      onClick={disable || isOrdered ? null : action}
      data-guid={guid}
      className={
        disable ? "disable place" : isOrdered ? "isOrdered place" : "place"
      }
    >
      {place}
    </span>
  );
};

export default Place;
