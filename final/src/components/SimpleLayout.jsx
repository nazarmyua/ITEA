import React from "react";
import { Route } from "react-router-dom";

const SimpleLayout = ({ children }) => {
  return (
    <div>
      <div>Simple layout</div>
      <div>{children}</div>
    </div>
  );
};

const SimpleLayoutRoute = ({ component: Component, ...routeParams }) => {
  return (
    <Route
      {...routeParams}
      render={(...match) => (
        <SimpleLayout>
          <Component {...match} />
        </SimpleLayout>
      )}
    />
  );
};

export default SimpleLayoutRoute;
