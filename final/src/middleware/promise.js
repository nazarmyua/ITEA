const middleware = (store) => (next) => (action) =>{
    if( action.type !== 'PROMISE'){
      return next(action);
    } else {
      const [startAction, successAction, failureAction] = action.actions;
      store.dispatch({
        type: startAction
      });    
      return action.promise.then( (responce) => store.dispatch({
        type: successAction,
        data: responce.data
      }) ).catch( (error) => store.dispatch({
        type: failureAction,
        error: error
      }));
    }
  };
  
  export default middleware;
  