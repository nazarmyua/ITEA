import { REQ_HALL_LIST, RES_HALL_LIST, ERROR_HALL_LIST } from "../constants";

const initialState = {
  loading: false,
  loaded: false,
  data: [],
  error: ""
};

function hallReducers(state = initialState, action) {
  switch (action.type) {
    case REQ_HALL_LIST:
      return {
        ...state,
        loading: true
      };
    case RES_HALL_LIST:
      return {
        ...state,
        data: action.data,
        loading: false,
        loaded: true
      };
    case ERROR_HALL_LIST:
      return {
        ...state,
        errors: action.error
      };
    default:
      return state;
  }
}

export default hallReducers;
