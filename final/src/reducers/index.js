import {combineReducers} from "redux"
import filmListReducers from "./filmListReducers"
import hallReducers from "./hallReducers"
import shopCardReducers from "./shopCardReducers"

let reducers = combineReducers({
    shopCardReducers,
    filmListReducers,
    hallReducers
})
export default reducers