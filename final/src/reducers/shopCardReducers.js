import { SHOP_CARD_ADD, SHOP_CARD_REMOVE,CLEAR_SHOP_CARD } from "../constants";

const initialState = {
  shopCard: []  
};

function hallReducers(state = initialState, action) {
  switch (action.type) {
    case SHOP_CARD_ADD:
    return {
      ...state,
      shopCard: [...state.shopCard, {row: action.row,place: action.place, guid: action.guid}]
    };
    case SHOP_CARD_REMOVE:
    return {
      ...state,
      shopCard: state.shopCard.filter((item)=>{return !(item.row==action.row && item.place==action.place && item.guid == action.guid)})
    };
    case CLEAR_SHOP_CARD:
    return {
      ...state,
      shopCard: []
    };
    default:
      return state;
  }
}

export default hallReducers;
