import { REQ_FILM_LIST, RES_FILM_LIST, ERROR_FILM_LIST } from "../constants";

const initialState = {
  loading: false,
  loaded: false,
  data: [],
  error: ""
};

function filmlistResucers(state = initialState, action) {
  switch (action.type) {
    case REQ_FILM_LIST:
      return {
        ...state,
        loading: true
      };
    case RES_FILM_LIST:
      return {
        ...state,
        data: action.data,
        loading: false,
        loaded: true
      };
    case ERROR_FILM_LIST:
      return {
        ...state,
        errors: action.error
      };
    default:
      return state;
  }
}

export default filmlistResucers;
