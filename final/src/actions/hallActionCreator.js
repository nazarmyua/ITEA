import { REQ_HALL_LIST, RES_HALL_LIST, ERROR_HALL_LIST} from "../constants";
import axios from "axios"

const hallActionCreator = {
  getHalls() {
    return function(dispatch, getState) {
      const url =
        "https://next.json-generator.com/api/json/get/VkFvXs7AE";
      dispatch({
        type: "PROMISE",
        actions: [REQ_HALL_LIST, RES_HALL_LIST, ERROR_HALL_LIST],
        promise: axios.get(url)
      });
    };
  }
};

export default hallActionCreator;

