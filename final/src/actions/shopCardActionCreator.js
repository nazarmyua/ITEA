import {SHOP_CARD_ADD,SHOP_CARD_REMOVE,CLEAR_SHOP_CARD} from "../constants"

const shopCardActionCreator = {
    addOrder(row,place,guid){
        return function(dispatch, getState){
        dispatch({
            type: SHOP_CARD_ADD,
            row,
            place,
            guid
        });
    }},
    removeOrder(row,place,guid){
        return function(dispatch, getState){
        dispatch({
            type: SHOP_CARD_REMOVE,
            row,
            place,
            guid
        });
    }},
    clearShopCard(){
        return function(dispatch, getState){
        dispatch({
            type: CLEAR_SHOP_CARD,
        });
    }},
};

export default shopCardActionCreator;