import { REQ_FILM_LIST, RES_FILM_LIST, ERROR_FILM_LIST} from "../constants";
import axios from "axios"

const filmListActionCreator = {
  getFilmList() {
    return function(dispatch, getState) {
      const url =
        "http://www.json-generator.com/api/json/get/cfcCApCHUy?indent=2";
      dispatch({
        type: "PROMISE",
        actions: [REQ_FILM_LIST, RES_FILM_LIST, ERROR_FILM_LIST],
        promise: axios.get(url)
      });
    };
  }
};

export default filmListActionCreator;

