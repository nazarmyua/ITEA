import { createStore, applyMiddleware, compose } from "redux";
import { CLEAR_SHOP_CARD} from "./constants"
import reducer from "./reducers";

import thunk from "redux-thunk";

import promise from "./middleware/promise";

let timer;

const composeEnhancers =
  process.env.NODE_ENV !== "production" &&
  typeof window !== "undefined" &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose;

const middleware = applyMiddleware(thunk, promise);

let store = createStore(reducer, composeEnhancers(middleware));

const changeHandler = () => {
  clearTimeout(timer)
  timer = setTimeout(() => {
    store.dispatch({type: CLEAR_SHOP_CARD})
  }, 60000);
};

store.subscribe(changeHandler);

export default store;
