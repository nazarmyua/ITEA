//FILM_LIST
export const REQ_FILM_LIST = "REQ_FILM_LIST"
export const RES_FILM_LIST = "RES_FILM_LIST"
export const ERROR_FILM_LIST = "ERROR_FILM_LIST"
//FILM_LIST


//HALL
export const REQ_HALL_LIST = "REQ_HALL_LIST"
export const RES_HALL_LIST = "RES_HALL_LIST"
export const ERROR_HALL_LIST = "ERROR_HALL_LIST"
//HALL

export const SHOP_CARD_ADD ="SHOP_CARD_ADD"
export const SHOP_CARD_REMOVE ="SHOP_CARD_REMOVE"
export const CLEAR_SHOP_CARD = "CLEAR_SHOP_CARD"