import React, { Component } from 'react';
import logo from './logo.svg';
import TabsComponent from './components/Tabs/TabsComponent'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <TabsComponent/>        
      </div>
    );
  }
}

export default App;
