import React from 'react'
import Button from "../Button/Button"
import Tab from "./TabComponent"
import Comp from "../Comp/Comp"

import "./TabsComponent.css"

class TabsComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            // Активный таб
            activeTabIndex: 1,
            tabs: [
                // Дочерним компонентом, будет другой stateless компонент в который можно передать другой элемент
                { id: 1, name: 'one', component: (<Tab>1</Tab>) },
                // Просто передаем строку
                { id: 2, name: 'two', component: 'Привет я строка которая будет переданна как дочерний элемент' },
                // Передаем stateless компонтент который не принимает никаких дочерних элементов
                { id: 3, name: 'three', component: (<Comp/>) },
                // Передаем выражение
                { id: 4, name: 'four', component: 120 + 500 - 300 },
            ]
        }    
    }

    changeAcviveTabIndex = ( e ) => {
        let newid = e.target.dataset.newid;
        console.log( 'newid', newid, e.target, e.target.dataset );
        this.setState({
            activeTabIndex: Number(newid)
        })
    }
    
    shouldComponentUpdate (nextProps, nextState) {
        return nextState.activeTabIndex !== this.state.activeTabIndex;            
    }

    render() {
        console.log("render")
        let {tabs,activeTabIndex} = this.state
        return (
            <div className="TabsComponent">
                <header>
                    {
                        tabs.map((item,key) => { return (
                                <Button
                                    key={key}
                                    className={activeTabIndex === item.id ? "active" : null} 
                                    id={item.id}
                                    onClick={this.changeAcviveTabIndex}
                                    >
                                        {item.name}
                                    </Button>) })
                    }
                </header>
                <section>
                    {
                       tabs.map((item,key) => { return activeTabIndex === item.id ? <Tab key={key}>{item.component}</Tab> : null})
                    }
                </section>
            </div>
        )
    }
}

export default TabsComponent