import React from 'react';
import './TabComponent.css'

const Tab = ({children}) => {
    return (
        <div className="Tab">{children}</div>        
    );
}

export default Tab
