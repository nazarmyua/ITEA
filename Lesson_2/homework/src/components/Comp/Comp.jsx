import React from 'react';

const Comp = () => {
    return (
        <p>Component without children</p>
    );
}

export default Comp;