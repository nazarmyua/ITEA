import React from "react"
import "./Button.css"

const Button = ({children,onClick,className, id}) =>{
    return <button className={className} data-newid={id} onClick={onClick}>{children}</button>
}

export default Button