import React from "react"

const ButtonStateLess = ({name,style,action}) => (
    <button style={style} onClick={action}>{name}</button>
)

export default ButtonStateLess