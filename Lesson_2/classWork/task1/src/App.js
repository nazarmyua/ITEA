import React, { Component } from 'react';
import logo from './logo.svg';
import StateLessButton from './BaseComponents/StateLessButton'
import './App.css';

class App extends Component {

  onAction = () => {
    alert("Action")
  }
  
  style = {
    display: "block",
    margin: "0 auto"
  }

  render() {
    let {style, onAction} = this;
    
    return (
      <div className="App">
        <StateLessButton name={"testname"} style={style} action={onAction}/>  
      </div>
    );
  }
}

export default App;
