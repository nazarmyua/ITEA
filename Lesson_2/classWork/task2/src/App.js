import React, { Component } from "react";
import logo from "./logo.svg";
import StateLessButton from "./BaseComponents/StateLessButton";
import "./App.css";

class App extends Component {
  constructor() {
    super();
    this.state = {};
  }
  componentDidMount() {
    fetch("http://www.json-generator.com/api/json/get/cqHYmWsHyq?indent=2")
      .then(res => res.json())
      .then(data => {
        let workArray = data.map(item => {
          return {
            interviewed: false,
            user: item
          };
        });
        this.setState({
          users: workArray
        });
      });
  }

  active={
    color:"green"
  }
  dibabled={
    color:"red"
  }
  changeState (workGuid) {
    var updatedUsers = this.state.users.map(element => {
      if(element.user.guid == workGuid)
        element.interviewed = !element.interviewed
      return element;        
    });
    this.setState({
      users: updatedUsers
    })
  }
  render() {
    let { users } = this.state;
    let {active, dibabled,changeState} = this
    return (      
      <div className="App">
        {users
          ? users.map((item,key) => {   
              let changeCurrentState  = changeState.bind(this,item.user.guid);
              return <StateLessButton key={key} style={item.interviewed?active:dibabled} action={changeCurrentState} name={item.user.name} />;
            })
          : "Users not found"}
      </div>
    );
  }
}

export default App;
