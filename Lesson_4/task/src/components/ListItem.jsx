import React from 'react';

const ListItem = ({match}) => {
    return (
        <div>List item {match.params.iditem}</div>
    );
}

export default ListItem;