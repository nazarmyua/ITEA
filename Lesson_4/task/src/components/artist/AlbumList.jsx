import React, { Component } from "react";
import { Link } from "react-router-dom";

class AlbumList extends Component {
  constructor() {
    super();
    this.state = {
      artistList: [],
      loading: false,
      loaded: false
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    let artistList = localStorage.getItem("artistList");
    if (artistList) {
      artistList = JSON.parse(artistList);
      this.setState({
        artistList: artistList,
        loaded: true,
        loading: false
      });
    } else {
      fetch("http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2")
        .then(response => response.json())
        .then(json => {
          this.setState({
            artistList: json,
            loaded: true,
            loading: false
          });
          localStorage.setItem("artistList", JSON.stringify(json));
        });
    }
  }
  render() {
    let { artistList, loading, loaded } = this.state;
    let artistName = this.props.match.params.name;
    if (loading) {
      return <div>Loading</div>;
    }
    if (!loaded) {
      return <div>Loaded</div>;
    }
    if (loaded && artistList) {
      let artist = this.state.artistList.filter(item => {
        return item.name == artistName;
      });
      return (
        <div>
          <ul>
            {artist[0].album.map((item, index) => {
              let sumDuration = item.compositions.reduce(
                (previusValue, curentItem) => {
                  return (
                    previusValue +
                    curentItem.duration.split(":")[0] * 60 +
                    --curentItem.duration.split(":")[1]
                  );
                },0);
              return (
                <li key={index}>
                  <Link to={`/artist/${artistName}/${item.name}`}>
                    {item.name} ({parseInt(sumDuration/60)}:{sumDuration%60})
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      );
    }
  }
}

export default AlbumList;
