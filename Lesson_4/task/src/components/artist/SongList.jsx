import React, { Component } from "react";
import { Link } from "react-router-dom";

class SongList extends Component {
  constructor() {
    super();
    this.state = {
      artistList: [],
      loading: false,
      loaded: false
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    let artistList = localStorage.getItem("artistList");
    if (artistList) {
      artistList = JSON.parse(artistList);
      this.setState({
        artistList: artistList,
        loaded: true,
        loading: false
      });
    } else {
      fetch("http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2")
        .then(response => response.json())
        .then(json => {
          this.setState({
            artistList: json,
            loaded: true,
            loading: false
          });
          localStorage.setItem("artistList", JSON.stringify(json));
        });
    }
  }

  render() {
    let { artistList, loading, loaded } = this.state;
    let { name, album } = this.props.match.params;
    if (loading) {
      return <div>Loading</div>;
    }
    if (!loaded) {
      return <div>Loaded</div>;
    }
    if (loaded && artistList) {
      let curentArtist = artistList.filter(item => {
        return item.name.toLowerCase() == name.toLowerCase();
      })[0];
      let curentAlbum = curentArtist.album.filter(item => {
        return item.name.toLowerCase() == album.toLowerCase();
      })[0];
      return (
        <div>
          <ul>
            {curentAlbum.compositions.map((item, index) => {
              return (
                <li key={index}>
                  {item.name} {item.duration}
                </li>
              );
            })}
          </ul>
        </div>
      );
    }
  }
}

export default SongList;
