import React, { Component } from "react";
import { Link } from "react-router-dom";

class ArtistList extends Component {
  constructor() {
    super();
    this.state = {
      artistList: [],
      loading: false,
      loaded: false
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    let artistList = localStorage.getItem("artistList");
    if (artistList) {
      artistList = JSON.parse(artistList);
      this.setState({
        artistList: artistList,
        loaded: true,
        loading: false
      });
    } else {
      fetch("http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2")
        .then(response => response.json())
        .then(json => {
          this.setState({
            artistList: json,
            loaded: true,
            loading: false
          });
          localStorage.setItem("artistList", JSON.stringify(json));
        });
    }
  }

  render() {
    let { artistList, loading } = this.state;
    if (loading) {
      return <div>Loading</div>;
    }
    return (
      <div>
        <ul>
          {artistList.map((item, index) => {
            return (
              <li key={index}>
                <Link to={`/artist/${item.name}`}>{item.name}</Link>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default ArtistList;
