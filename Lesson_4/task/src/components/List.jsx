import React from 'react';
import {Link} from "react-router-dom"

const List = () => {
    let temp = new Array(20).fill(0)
    return (
        <div>
            <div>List</div>
            <div>
            <ul>
            {
                temp.map((_, index)=>{
                    return <li ley={index}><Link to={`/list/${index}`}>{index}</Link></li>
                })
            }
            </ul>
            </div>
        </div>
    );
}

export default List;