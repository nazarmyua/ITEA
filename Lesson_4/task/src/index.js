import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { BrowserRouter, NavLink, Route, Switch } from "react-router-dom";

import Home from "./components/Home";
import About from "./components/About";
import Contacts from "./components/Contacts";
import List from "./components/List";
import ListItem from "./components/ListItem";
import NotFound from "./components/NotFound"

import ArtistList from "./components/artist/ArtistList"
import AlbumList from "./components/artist/AlbumList"
import SongList from "./components/artist/SongList"

import registerServiceWorker from "./registerServiceWorker";
const supportsHistory = "pushState" in window.history;

ReactDOM.render(
  <BrowserRouter forceRefrech={!supportsHistory}>
    <div>
    <ul>
      <li><NavLink to="/">Home</NavLink></li>
      <li><NavLink to="/about">About</NavLink></li>
      <li><NavLink to="/contacts">Contacts</NavLink></li>
      <li><NavLink to="/list">List</NavLink></li>
      <li><NavLink to="/artist">Atrists</NavLink></li>
      </ul>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/about" component={About} />
      <Route exact path="/contacts" component={Contacts} />
      <Route exact path="/list" component={List} />
      <Route exact path="/list/:iditem" component={ListItem} />
      <Route exact path="/artist" component={ArtistList}/>
      <Route exact path="/artist/:name" component={AlbumList}/>
      <Route exact path="/artist/:name/:album" component={SongList}/>      

      <Route component={NotFound}/>
    </Switch>      
    </div>
  </BrowserRouter>,
  document.getElementById("root")
);
registerServiceWorker();
