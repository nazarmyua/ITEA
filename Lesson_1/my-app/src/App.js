import React, { Component } from "react";
//import logo from "./application.jpg";
import Guest from "./guests.json";
import search from "./search.svg";
import "./App.css";

console.log(Guest);
class App extends Component {
  constructor() {
    super();
    this.state = {
      guestList: Guest,
      visibleSearch: false
    };

    this.changeVisibleSearchState = this.changeVisibleSearchState.bind(this);
  }
  changeVisibleSearchState() {
    this.setState({
      visibleSearch: !this.state.visibleSearch
    });
  }
  filterGuests = event => {
    let searchValue = event.target.value.toLowerCase();
    let filtered = Guest.filter(item => {
      let itemName = item.name.toLowerCase();
      return itemName.indexOf(searchValue) !== -1;
    });
    this.setState({
      guestList: filtered
    });
  };
  render() {
    let { visibleSearch } = this.state;
    let { guestList } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <div>
            <span className="second-logo">Список жертв</span>
            <span className="logo">Список гостей</span>
          </div>
          <button
            className="search-button"
            onClick={this.changeVisibleSearchState}
          >
            <img src={search} alt="search" />
          </button>
        </header>
        {visibleSearch === true ? (
          <div className="App-search">
            <input onChange={this.filterGuests} />
          </div>
        ) : null}

        <div className="items">
          {guestList.map((item, key) => <Item item={item} key={key} />)}
        </div>
      </div>
    );
  }
}
class Item extends Component {
  constructor() {
    super();
    this.state = {
      arrived: false
    };
  }
  changeArrivedState = () => {
    console.log("test");
    this.setState({
      arrived: !this.state.arrived
    });
  };
  render() {
    let { name, company, phone, address } = this.props.item;
    let { arrived } = this.state;
    return (
      <div className="item">
        <div>
          <p>
            Гість
            <b>
              <span className={arrived ? "arriver" : null}> {name}</span>
            </b>
            працює в компанії <b>"{company}"</b>.
          </p>
          <p>Його контакти:</p>
          <p>
            <b>{phone}</b>
          </p>
          <p>
            <b>{address}</b>
          </p>
        </div>
        <div>
          <button onClick={this.changeArrivedState}>Прибув</button>
        </div>
      </div>
    );
  }
}

export default App;
